
fun main() {
    rangeFatorial(0, 10)
    // calcFatorial(5)
}

/**
 * Fatorial de 5
 * 5 x 4 x 3 x 2 x 1 = 120
 **/
fun calcFatorial(value: Int) {

    // armazena o resultado do fatorial de cada looping
    var result: Int = 1

    // looping responsavel por calcular o fatorial de cada numero, o metodo 'downTo' dispoe
    // os números do maior para o menor.
    for (i in value downTo 1) {

        print("$i")
        // Aqui é multiplicado o antecessor com o resultado da multiplicação anterior
        result *= i

        // condição utilizada mais para apresentar a informção na tela
        if (i > 1) {
            print(" x ")
        } else {
            println(" = $result")
        }
    }
}

/**
 * Fatorial de 0 a 10
 * 1 = 1
 * 2 x 1 = 2
 * 3 x 2 x 1 = 6
 * 4 x 3 x 2 x 1 = 24
 * 5 x 4 x 3 x 2 x 1 = 120
 * 6 x 5 x 4 x 3 x 2 x 1 = 720
 * 7 x 6 x 5 x 4 x 3 x 2 x 1 = 5040
 * 8 x 7 x 6 x 5 x 4 x 3 x 2 x 1 = 40320
 * 9 x 8 x 7 x 6 x 5 x 4 x 3 x 2 x 1 = 362880
 * 10 x 9 x 8 x 7 x 6 x 5 x 4 x 3 x 2 x 1 = 3628800
 * */

fun rangeFatorial(min: Int, max: Int) {
    // looping para calcular o fatorial de todos os numeros
    // ex. calcula o fatorial de todos os numeros entre 0 á 10.
    for (x in min .. max){

        // chama a função calcula fatorial
        calcFatorial(x)
    }
}



