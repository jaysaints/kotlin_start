fun sayHello3(greeting:String,  vararg name:String) {
    name.forEach { name ->
        println("$greeting $name")
    }
}

fun main() {
    val names = arrayOf("Jay", "Mary", "July")
    sayHello3(greeting = "Hello", name = names)
}