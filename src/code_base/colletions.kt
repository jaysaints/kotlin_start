
fun arrayCollection () {
    val interestingThings = arrayOf("Kotlin", "Banana", "Books")

    println(interestingThings.size)
    println(interestingThings[0])
    println(interestingThings.get(0))

    for (interstingThing in interestingThings) {
    println(interstingThing)
    }

    interestingThings.forEach {
    println(it)
    }
    // or
    interestingThings.forEach { interestingThing ->
    println(interestingThing)
    }

    interestingThings.forEachIndexed { index, interestingThing ->
    println("index: $index item: $interestingThing")
    }

    // Add new element
    val interestingThings2 = mutableListOf("Kotlin", "Banana", "Books")
    interestingThings2.add("Monkey")
}

fun mapInteraction () {
    val map1 = mapOf(1 to "a", 2 to "b", 3 to "c")
    map1.forEach { key, value -> println("key: $key value: $value")}

    val map2 = mutableMapOf(1 to "a", 2 to "b", 3 to "c")
    map2.put(4, "d")
    map2.forEach { key, value -> println("key: $key value: $value")}
}

fun sayHi (greeting: String, name: List<String>) {
    name.forEach { name ->
        println("$greeting $name")
    }
}

fun main() {
    // arrayCollection()
    // mapInteraction()

    val names = listOf<String>("Jay", "Mary", "Jully")
    sayHi("Hi", names)
}