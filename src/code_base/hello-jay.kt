fun sayHello(name: String) {
    println("Hello $name")
}

fun main() {
    sayHello("Jay")
}