/*******************************************************************************
VARIABLE
 * --> `val`
 * val a: Int = 1   - Immediate assignment
 * val b = 2        - `Int` type is inferred
 * val c: Int       - Type required when no initializer is provided
 * c = 3            - Deferred assignment

 * --> `var`
 * Variables that can be reassigned use the var keyword.
 * var x = 5 // `Int` type is inferred
// x += 1
 *******************************************************************************/

fun varVariable() {
    var a = 5
    println("1º value of a: $a")

    a = 100
    println("2º value of a: $a")

    // Obs.: Cannot change variable type
}

fun valVariable() {
    val a = "Type String"
    println("1º value of a: $a")

    // a = "Type String" <-- This is an error
    // println("2º value of a: $a")

    // Obs.: Variable val cannot be reassigned
}


fun main() {
    varVariable()
}