// A class in Kotlin can have a primary constructor and one or more secondary constructors.
// The primary constructor is a part of the class header, and it goes after the class name and optional type parameters.

//class Person(_firstName: String, _lastName: String) {
//    val firstName: String = _firstName
//    val lastName: String = _lastName
//}
// A class is 'Public' for default. But it can be Private, Internal
class Person(val firstName: String = "Jay", val lastName: String = "Saints") {

//    init {
//        println("init 1")
//    }
//    constructor(): this("Peter", "Parker"){
//        println("Secondary Constructor")
//    }
//
//    init {
//        println("init 2")
//    }

    // A property can be protected or private for example:
    // protected var nickName: String? = null
    var nickName: String? = null
        set(value) {
            field = value
            // println("the new nockname is $value")
        }
        get() {
            // println("the returned value is $field")
            return field
        }

    fun printInfo() {
        val nickNameToPrint = nickName ?: "no nickname"
        println("$firstName ($nickNameToPrint) $lastName")
    }
}


//fun main() {
//    // Initialize with first constructor
//    val p1 = Person("Peter", "Parken")
//    println("${p1.firstName} ${p1.lastName}")
//
//    // Initialize with second constructor
//    val p2 = Person()
//    println("${p2.firstName} ${p2.lastName}")
//    p2.printInfo()
//}