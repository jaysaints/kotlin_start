// Default function structure
fun sum1(n1: Int, n2: Int): Int {
    return n1 + n2
}

// Simplify the function body
fun sum2(n1: Int, n2: Int) = n1 + n2

// Function that returns no meaningful value
// Use variables between strings
fun sum3(n1: Int, n2: Int): Unit {
    println("sum of $n1 and $n2 is ${n1 + n2}")
}

//fun sayHello(greeting: String, name: String) = println("$greeting $name")



// Main function start the script
fun main() {
    // sayHello(name = "Jay", greeting = "Hello")


    val result = sum1(10, 10)
    println(result)
    sum3(15, 5)

}