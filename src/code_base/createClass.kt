package code_base // A class in Kotlin can have a primary constructor and one or more secondary constructors.
 // The primary constructor is a part of the class header, and it goes after the class name and optional type parameters.
 class Person constructor(var name: String, var age: Int){
     var infoPeople = "Name: $name\nAge: $age"
 }


 fun main() {
     // Instance class
     val p1 = Person("Jay Saints", 25)
     println(p1.infoPeople)
 }


